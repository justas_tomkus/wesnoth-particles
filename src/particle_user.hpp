/* $Id$ */
/*
   Copyright (C) 2012 by Justas Tomkus <justas.tomkus@gmail.com>
   Part of the Battle for Wesnoth Project http://www.wesnoth.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY.

   See the COPYING file for more details.
*/


#ifndef PARTICLE_USER_HPP_INCLUDED
#define PARTICLE_USER_HPP_INCLUDED

#include <string>
#include <vector>
#include <map>

class effect;
class map_location;
class config;

class base_user {
	friend class effect;
public:
	base_user();
	virtual ~base_user();
	std::vector<effect* > eff_list_;
	virtual int remove_effect();
	virtual void update_effect_position(const map_location& loc);
	virtual void update_effect_position(int x_off=0, int y_off=0);
	virtual void start_effect();
	virtual void start_effect(const map_location& loc);
};

class effect_user : public base_user {
	friend class unit_animation;
	friend class unit_animator;
public:
	effect_user();
	virtual ~effect_user();
	effect_user(const config& cfg);
	
};

class terr_eff_user : public base_user {
	friend class effect;
public:
	terr_eff_user();
	terr_eff_user(const terr_eff_user& eff);
	virtual ~terr_eff_user();
	virtual void add_effect(const config& cfg, const map_location& loc);
	virtual void stop_effect();
};

class wml_effect_user : public base_user {
	friend class effect;
public:
	wml_effect_user();
	std::map<int, effect*> eff_list_;
	virtual ~wml_effect_user();
	virtual int add_effect(const config& cfg, const map_location& loc, int id);
	virtual void update_effect_position(const map_location& loc, int id);
	virtual void start_effect(int id);
	virtual void stop_effect(int id);
private:
	static int id_gen_;
};


#endif // PARTICLE_USER_HPP_INCLUDED
