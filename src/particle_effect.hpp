/* $Id$ */
/*
   Copyright (C) 2012 by Justas Tomkus <justas.tomkus@gmail.com>
   Part of the Battle for Wesnoth Project http://www.wesnoth.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY.

   See the COPYING file for more details.
*/

#ifndef PARTICLE_EFFECT_HPP_INCLUDED
#define PARTICLE_EFFECT_HPP_INCLUDED

#include "sdl_utils.hpp"
//#include "config.hpp"
#include "particle_engine.hpp"
class particle;
class effect_user;
//class effect_configurator;
class config;

// effect itself
class effect {
public:
	int active_;
	base_user* user_;
	config cfg;
	std::vector<particle* > particle_list_;
	// surface to be added to disp->drawing_buffer
	surface effect_surf_;
	// used for invalidations
	SDL_Rect surf_rect_;
	// size of particles to be displayed on screen
	SDL_Rect part_display_size_;
	int origin_x;
	int origin_y;
	bool follow_;
	~effect();
	effect();
	void update_particle_display_size();
	void populate_particles(int dt);
	inline void populate_particles(int x, int y);
	void shift_particles(int dx, int dy);
	bool operator==(const effect& eff) const {
		return this == &eff;
	};
	static const int KILL = 1<<1;
	static const int ACTIVE = 1;
	static const int INVALIDATE = 1<<2;
	static const int MODEL = 1<<3;
	static const int INSTKILL = 1<<4;
	static const int TERRAIN = 1<<5;

	// former effect type
	unsigned int particle_number_;
	int particle_lifetime_;
	int lifetime_var_;
	int emission_time_;
	float part_to_emit_;
	float spread_;
	std::vector<Uint32> color_array_;
//	std::string particle_formula_;
//	int frames_number_;
	SDL_Rect particle_size_;
//	void (*movement_f)(particle&, int&, int&, float);
	effect_configurator::func_t* movement_f;
};

#endif // PARTICLE_EFFECT_HPP_INCLUDED
