/* $Id$ */
/*
   Copyright (C) 2012 by Justas Tomkus <justas.tomkus@gmail.com>
   Part of the Battle for Wesnoth Project http://www.wesnoth.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY.

   See the COPYING file for more details.
*/


#ifndef PARTICLE_ENGINE_HPP_INCLUDED
#define PARTICLE_ENGINE_HPP_INCLUDED

#include "display.hpp"
#include "map_location.hpp"
#include "sdl_utils.hpp"

class particle_engine;

class particle;
class effect;

template <class Tclass> class functor {
	Tclass* pt_object;
	void (Tclass::*fpointer)(particle&, effect&, int);

public:
	functor() {};
	functor(Tclass* pt_obj, void(Tclass::*fpt)(particle&, effect&, int)) :
		pt_object(pt_obj), fpointer(fpt) {};
	void operator()(particle& part, effect& e, int t) {
		(*pt_object.*fpointer)(part, e, t);
	};
};

class effect_configurator;
class effect_configurator {
	void fill_movement_map();
public:
	typedef functor<effect_configurator> func_t;
	std::map<std::string, func_t> func_map;

	effect_configurator();
	void test_move_(particle& part, effect& eff, int dtime=160);
	void fire_move_(particle& part, effect& eff, int dtime=160);
	void contract_move_(particle& part, effect& eff, int dtime=160);
	void expand_move_(particle& part, effect& eff, int dtime=160);
	void wavyf_move_(particle& part, effect& eff, int dtime=160);
	void rise_move_(particle& part, effect& eff, int dtime=160);
	void spiral_move_(particle& part, effect& eff, int dtime=160);
	void star_move_(particle& part, effect& eff, int dtime=160);
	void spray_move_(particle& part, effect& eff, int dtime=160);
	void ripple_move_(particle& part, effect& eff, int dtime=160);
	void wave_move_(particle& part, effect& eff, int dtime=160);

};


class particle {
public:
	particle();
	particle(int x, int y, Uint32 color = 0xffffffff);
	~particle();
	void renew(const SDL_Rect& dims, double last_zoom, double current_zoom);
	void recreate_surface(int w, int h);
	inline void shift(int dx, int dy) { x+=dx; y+=dy; };
	inline int X() const { return static_cast<int>(x); };
	inline int Y() const { return static_cast<int>(y); };
	inline int Time() const { return time; };

	inline void recolor(Uint32 newcol);
	void respawn(int cx, int cy);
	void respawn(int cx, int cy, const effect& eff);
	void draw();
	friend class particle_engine;
	friend class effect_configurator;
private:
	float x;
	float y;
	// how long particle is alive so far (in ms)
	int time;
	int dx;
	int dy;
	float* extra;
	Uint32 color;
	surface surf_;
};


class effect_user;
class config;



class particle_engine
{
	particle_engine();
public:
	~particle_engine();
	inline static particle_engine* get_singleton() {
		return singleton_ ? singleton_ : (new particle_engine());
	}
	void update(int ctime);
	void reset();
	// model==false adds effect normaly (used for terrain)
	effect* add_model_effect(const config& cfg,
					base_user& eu, bool model=true);
	effect* copy_effect(effect& eff, base_user& eu);
	bool effect_exists(effect* effp);
	void move_effect(effect& eff, const map_location& loc);
	void move_effect(effect& eff, int x_off=0, int y_off=0);
	int remove_effect(effect& eff);
	void activate_effect(effect& eff);
	void invalidate_effect(const effect& eff);
	void invalidate();
	void cleanout();
	inline void collect_effects() {
		++effect_removed_;
	};
	inline void collect_now() {
		effect_removed_ += 43;
	};
	// used to deffer move functionality, as it requires display, which is being
	// built at the time terrain effects are added.
	void defer_move(effect& eff, const map_location& loc);
	// debug
	int act_eff();
private:
	void apply_defers();
	effect_configurator effect_config_;
	void update_effect(effect& eff, int time_slice);

	static particle_engine* singleton_;
	std::list< effect* > effect_list_;
//	std::vector< effect* > active_list_;
	double last_zoom_;
	int last_time_;
	int last_delta_;
	int effect_removed_;

	// deferring stuff
	bool do_defers_;
	struct defer_info {
		defer_info(effect& eff, const map_location loc) :
			effp(&eff), loc(loc) {};
		effect* effp;
		map_location loc;
	};
	std::vector<defer_info> defer_list_;
};

#endif // PARTICLE_ENGINE_HPP_INCLUDED
