/* $Id$ */
/*
   Copyright (C) 2012 by Justas Tomkus <justas.tomkus@gmail.com>
   Part of the Battle for Wesnoth Project http://www.wesnoth.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY.

   See the COPYING file for more details.
*/

#include "particle_engine.hpp"
#include "foreach.hpp"
#include <stdlib.h>
#include "particle_effect.hpp"
#ifndef PRAND
#define PRAND() (static_cast<float>(rand()) / RAND_MAX)
#define MATHPI 3.14159
#endif

// mini helper to cast hex string to int
template<typename T>
inline T lexical_hex_cast(const std::string& in) {
	T ret;
	std::stringstream ss;
	ss << std::hex << in;
	ss >> ret;
	return ret;
}

Uint32 interpolate_color(Uint32 c1, Uint32 c2, float ratio) {
	Uint32 a = (c1&0xff000000)*(1-ratio)+(c2&0xff000000)*ratio;
	Uint32 r = (c1&0xff0000)*(1-ratio)+(c2&0xff0000)*ratio;
	Uint32 g = (c1&0xff00)*(1-ratio)+(c2&0xff00)*ratio;
	Uint32 b = (c1&0xff)*(1-ratio)+(c2&0xff)*ratio;
	return (a&0xff000000) | (r&0xff0000) | (g&0xff00) | (b&0xff);
}

void interp_color(const effect& eff, particle& part) {
	if(eff.color_array_.size() > 1) {
		float tp = part.Time() / static_cast<float>(eff.particle_lifetime_);
		tp = tp < 0. ? 0. : tp;
		float ip = tp * (eff.color_array_.size()-1);
		int lower = floor(ip);
		Uint32 rez = interpolate_color(eff.color_array_[lower], eff.color_array_[ceil(ip)], ip-lower);
		part.recolor(rez);
	}
}


effect_configurator::effect_configurator() :
	func_map()
{
	fill_movement_map();
}

void effect_configurator::fill_movement_map() {
	func_map["test_move"] = effect_configurator::func_t(
			this, &effect_configurator::test_move_);
	func_map["fire_move"] = effect_configurator::func_t(
			this, &effect_configurator::fire_move_);
	func_map["contract_move"] = effect_configurator::func_t(
			this, &effect_configurator::contract_move_);
	func_map["expand_move"] = effect_configurator::func_t(
			this, &effect_configurator::expand_move_);
	func_map["wavyf_move"] = effect_configurator::func_t(
			this, &effect_configurator::wavyf_move_);
	func_map["rise_move"] = effect_configurator::func_t(
			this, &effect_configurator::rise_move_);
	func_map["spiral_move"] = effect_configurator::func_t(
			this, &effect_configurator::spiral_move_);
	func_map["star_move"] = effect_configurator::func_t(
			this, &effect_configurator::star_move_);
	func_map["spray_move"] = effect_configurator::func_t(
			this, &effect_configurator::spray_move_);
	func_map["ripple_move"] = effect_configurator::func_t(
			this, &effect_configurator::ripple_move_);
	func_map["wave_move"] = effect_configurator::func_t(
			this, &effect_configurator::wave_move_);

}

// -------------------------- movement functions ------------------------------
void effect_configurator::test_move_(particle& part, effect& eff, int dtime) {
	part.x = part.x + 1;
	part.y = part.y + 1;
	part.time += dtime;
}

void effect_configurator::fire_move_(particle& part, effect& eff, int dtime) {
	if(NULL == part.extra) {
		float f = eff.spread_ - (static_cast<float>(rand()) / RAND_MAX) * eff.spread_;
		f = rand()&1 ? -f : f;
		part.extra = new float(f);
	}
	float dx =
		cos((*part.extra) + (*part.extra) * eff.spread_ * part.time / 1000) ;
	part.x = part.x + dx;
	part.y = part.y - 1;
	part.time += dtime;
}

void effect_configurator::contract_move_(particle& part, effect& eff, int dtime) {
	float r = eff.cfg["radius"];
	if(NULL == part.extra) {
		float f = PRAND() * MATHPI * 2;
		part.x = cos(f) * r + eff.origin_x;
		part.y = sin(f) * r + eff.origin_y;
		part.extra = new float(f+MATHPI);
		part.time = PRAND() * 2 * eff.lifetime_var_ - eff.lifetime_var_;
	}
	r = dtime / eff.particle_lifetime_ * r;
	part.x += cos(*part.extra) * r;
	part.y += sin(*part.extra) * r;
	part.time += dtime;
}

void effect_configurator::expand_move_(particle& part, effect& eff, int dtime) {
	int r = eff.cfg["radius"];
	if(NULL == part.extra) {
		float f = PRAND() * MATHPI * 2;
		part.extra = new float(f);
		part.x = eff.origin_x;
		part.y = eff.origin_y;
		part.time = PRAND() * 2 * eff.lifetime_var_ - eff.lifetime_var_;
	}
	float t = static_cast<float>(dtime)/eff.particle_lifetime_ * r;
	part.x += cos(*part.extra) * t;
	part.y += sin(*part.extra) * t;
	part.time += dtime;
}

// not good for moving effects
void effect_configurator::wavyf_move_(particle& part, effect& eff, int dtime) {
	if(NULL == part.extra) {
		int r = eff.cfg["radius"];
		int offy = eff.cfg["offset_y"];
		float f = PRAND() * eff.spread_ - eff.spread_/2;
		part.extra = new float(f);
		part.x = eff.origin_x + sin(f) * r;
		part.y = eff.origin_y - offy;
		part.time = PRAND() * 2 * eff.lifetime_var_ - eff.lifetime_var_;
	}
	part.y += dtime / 10.;
	float dx = eff.spread_ * sin(
	*part.extra * (part.y - eff.origin_y));
	float max_var = eff.cfg["maxx"];
	if(fabs(part.x + dx - eff.origin_x) < max_var) {
		part.x += dx;
	}
	part.time += dtime;
}

void effect_configurator::rise_move_(particle& part, effect& eff, int dtime) {
	if(NULL == part.extra) {
		float f = PRAND() * MATHPI;
		part.extra = new float(f);
		int wide = eff.cfg["wide"];
		part.x = eff.origin_x + sin(f) * wide - wide/2;
		int offy = eff.cfg["offset_y"];
		part.y = eff.origin_y + offy;
		part.time = PRAND() * 2 * eff.lifetime_var_ - eff.lifetime_var_;
	}
	part.y -= dtime/5.;
	part.x += PRAND() - 0.5;
	part.time += dtime;
}

void effect_configurator::spiral_move_(particle& part, effect& eff, int dtime) {
	if(NULL == part.extra) {
		float f = PRAND();
		part.extra = new float(f);
		part.time = PRAND() * 2 * eff.lifetime_var_ - eff.lifetime_var_;
		part.x = eff.origin_x;
		part.y = eff.origin_y;
		part.time += dtime;
		return;
	}
	int r = eff.cfg["radius"];
	part.x += r/2*( (sin(part.time/100. + *part.extra) * part.time/1000.) -
		sin((part.time-dtime)/100. + *part.extra) * (part.time-dtime)/1000.);
	part.y += r/2*( (cos(part.time/100. + *part.extra) * part.time/1000.) -
		cos((part.time-dtime)/100. + *part.extra) * (part.time-dtime)/1000.);
	part.time += dtime;
}

void effect_configurator::star_move_(particle& part, effect& eff, int dtime) {
	int steps = eff.cfg["steps"];
	int r = eff.cfg["radius"];
	steps = steps ? steps : 1;
	if(NULL == part.extra) {
		float dir = eff.cfg["direction"].to_double();
		float f = dir + MATHPI*2/steps*(rand()%steps);
		part.extra = new float(f);
		part.x = eff.origin_x + cos(f) * r;
		part.y = eff.origin_y + sin(f) * r;
	}
	float ratio = static_cast<float>(part.time)/eff.particle_lifetime_;
	float mratio = ratio > 0.6 ? 0.5 * ratio : -2*ratio;
	part.x += cos(MATHPI*(1.1+mratio)+*part.extra - mratio) /2*
		(1 - ratio*ratio);
	part.y += sin(MATHPI*(1.1+mratio)+*part.extra - mratio) /2*
		(1 - ratio*ratio);
	part.time += dtime;
}

void effect_configurator::spray_move_(particle& part, effect& eff, int dtime) {
	int steps = eff.cfg["steps"];
	int r = eff.cfg["radius"];
	steps = steps ? steps : 1;
	if(NULL == part.extra) {
		float dir = MATHPI*eff.cfg["direction"].to_double();
		float f = dir + MATHPI*eff.spread_*(PRAND()-0.5);
		part.extra = new float(f);
		part.x = eff.origin_x;
		part.y = eff.origin_y;
	}
	float currat = static_cast<float>(dtime)/eff.particle_lifetime_ * r;
	part.x += cos(*part.extra + eff.spread_*(PRAND()-0.5)) *currat;
	part.y += sin(*part.extra + eff.spread_*(PRAND()-0.5)) *currat;
	part.time += dtime;
}

void effect_configurator::ripple_move_(particle& part, effect& eff, int dtime) {
	int steps = eff.cfg["steps"];
	if(NULL == part.extra) {
		float f = MATHPI*2*PRAND();
		part.extra = new float(f);
		part.x = eff.origin_x;
		part.y = eff.origin_y;
	}
	int r = eff.cfg["radius"];
	float ratio = static_cast<float>(part.time)/eff.particle_lifetime_;
	float currat = 0.5 + (cos(steps*MATHPI*2*ratio));
	currat *= static_cast<float>(dtime)/eff.particle_lifetime_ * r;
	part.x += cos(*part.extra) *currat;
	part.y += sin(*part.extra) *currat;
	part.time += dtime;
}

void effect_configurator::wave_move_(particle& part, effect& eff, int dtime) {
	int r = eff.cfg["radius"];
	if(NULL == part.extra) {
		float f = 2*MATHPI*(PRAND()-0.5);
		part.extra = new float(f);
		part.x = eff.origin_x + cos(f)*r*(PRAND()-0.5);
		part.y = eff.origin_y + sin(f)*r*(PRAND()-0.5);
	}
	float ratio = static_cast<float>(part.time)/eff.particle_lifetime_;
	float currat = static_cast<float>(dtime)/eff.particle_lifetime_ * r;
	part.x += sin(4*MATHPI*ratio) * currat;
	part.time += dtime;
}




// -------------------------- particles ---------------------------------------
particle::particle() :
	x(0),
	y(0),
	time(0),
	dx(1),
	dy(1),
	extra(NULL),
	color(0xffffffff)
{
	recreate_surface(2,2);
}
particle::particle(int x, int y, Uint32 color) :
	x(x),
	y(y),
	time(0),
	dx(1),
	dy(1),
	extra(NULL),
	color(color)
{
	recreate_surface(2,2);
}
particle::~particle() {
	if(extra) {
		delete extra;
		extra = NULL;
	}
}

void particle::renew(const SDL_Rect& dims, double last_zoom, double current_zoom) {
	recreate_surface(dims.w, dims.h);
	x = static_cast<int>(x / last_zoom * current_zoom);
	y = static_cast<int>(y / last_zoom * current_zoom);
}

void particle::recreate_surface(int w, int h) {
	surf_ = create_neutral_surface(w, h);
	recolor(color);
}

inline void particle::recolor(Uint32 newcol) {
	color = newcol;
	sdl_fill_rect(surf_, NULL, newcol);
}

void particle::respawn(int cx, int cy) {
	time = 0;
	x = cx;
	y = cy;
	if(extra) {
		delete extra;
		extra = NULL;
	}
}

void particle::respawn(int cx, int cy, const effect& eff) {
	time = rand() % (2*eff.lifetime_var_) - eff.lifetime_var_;
	x = cx;
	y = cy;
	if(extra) {
		delete extra;
		extra = NULL;
	}
}

void particle::draw() {
}


// -------------------------- particle engine ---------------------------------
particle_engine::particle_engine() :
	effect_config_(),
	effect_list_(),
//	active_list_(),
	last_zoom_(0),
	last_time_(SDL_GetTicks()),
	last_delta_((0u-1)/2),
	effect_removed_(0)
{
	singleton_ = this;
}

particle_engine::~particle_engine() {
	singleton_ = NULL;
	foreach(effect*& eff, effect_list_) {
		delete eff;
	}
}


void particle_engine::update_effect(effect& eff, int time_slice) {
	display* disp = display::get_singleton();
	double current_zoom = disp->get_zoom_factor();
	SDL_Rect map = disp->max_map_area();


	//check if zoom chaged, if so, recreate particles' surface and change
	//their position, so it's consistent
	if(fabs(last_zoom_ - current_zoom) > 0.0001) {
		eff.update_particle_display_size();
		foreach(particle*& part, eff.particle_list_) {
			part->renew(eff.part_display_size_, last_zoom_, current_zoom);
		}
		eff.origin_x = static_cast<int>(eff.origin_x / last_zoom_ * current_zoom);
		eff.origin_y = static_cast<int>(eff.origin_y / last_zoom_ * current_zoom);
	}
	eff.populate_particles(time_slice);
	if(!eff.particle_list_.size()) {
		// we don't want to search for max/min values, when there are no
		// particles in list
		return;
	}


	int minx = (0u-1)/2;;//x7fffffff;
	int maxx = (0u-1)/2+1;
	int miny = (0u-1)/2;;//x7fffffff;
	int maxy = (0u-1)/2+1;
	foreach(particle*& part, eff.particle_list_) {
		(*(eff.movement_f))((*part), eff, time_slice);
		// hack for direction changing
		part->dx += 2*(part->X()<0) - 2*(part->X()>map.w);
		part->dy += 2*(part->Y()<0) - 2*(part->Y()>map.h);
		// searching for bounding box of effect
		minx = minx < part->X() ? minx : part->X();
		// alternative w/o branching: bool t=maxx>part.x; maxx=maxx*t+(1-t)*part.x;
		maxx = maxx > part->X() ? maxx : part->X();
		miny = miny < part->Y() ? miny : part->Y();
		maxy = maxy > part->Y() ? maxy : part->Y();
		interp_color(eff, *part);
	}
	// effect surface size
	int sw = maxx - minx + eff.part_display_size_.w;
	int sh = maxy - miny + eff.part_display_size_.h;
	if(eff.surf_rect_.w < sw || eff.surf_rect_.h < sh) {
		eff.surf_rect_.w = sw;
		eff.surf_rect_.h = sh;
		eff.effect_surf_ = create_neutral_surface(sw, sh);
	}

	// write particles to effect surface
	sdl_fill_rect(eff.effect_surf_, NULL, 0x00ffffff);
	foreach(particle* const& part, eff.particle_list_) {
		eff.part_display_size_.x = part->X() - minx;
		eff.part_display_size_.y = part->Y() - miny;
		blit_surface(part->surf_, NULL, eff.effect_surf_,
				&eff.part_display_size_);
	}


	// writes effect to screen
	eff.surf_rect_.x = minx - disp->screen_position_x();
	eff.surf_rect_.y = miny - disp->screen_position_y();
	map_location loc = disp->pixel_position_to_hex(minx, miny);
	disp->drawing_buffer_add(display::LAYER_PARTICLES_EFFECT_FG, loc,
			eff.surf_rect_.x,
			eff.surf_rect_.y,
			eff.effect_surf_);
}

void particle_engine::update(int ctime) {
	if(!last_zoom_) {
		last_zoom_ = display::get_singleton()->get_zoom_factor();
	}
	// move terrain effects at the start of map
	if(do_defers_) {
		apply_defers();
	}
	display* disp = display::get_singleton();
	double current_zoom = disp->get_zoom_factor();
	last_delta_ = std::min(ctime - last_time_, last_delta_);
	// TODO: change ugly hardcoded constant
	if(effect_removed_ > 42) {
		cleanout();
		effect_removed_ = 0;
	}
	foreach(effect*& eff, effect_list_) {
		if(eff->active_&effect::ACTIVE) {
			// if effect is active, it should be invalidated later
			update_effect(*eff, last_delta_);
			eff->active_ |= effect::INVALIDATE;
		}
	}
	if(fabs(last_zoom_ - current_zoom) > 0.0001) {
		last_zoom_ = current_zoom;
	}
	last_time_ = ctime;
}

// removes every effect, except for MODEL effects
void effect_resetter(effect*& effp) {
	if(!((effp->active_&effect::MODEL) ||
		(effp->active_&effect::TERRAIN))) {
		delete effp;
		effp = NULL;
	}
}

void particle_engine::reset() {
	// clean out effects
	std::for_each(effect_list_.begin(), effect_list_.end(), effect_resetter);
	std::list<effect*>::iterator end =
	std::remove(effect_list_.begin(), effect_list_.end(), static_cast<effect*>(NULL));
	effect_list_.erase(end, effect_list_.end());
}

void particle_engine::invalidate() {
	// while testing, there is only one effect
	foreach(effect*& eff, effect_list_) {
		if(eff->active_&effect::INVALIDATE) {
			invalidate_effect(*eff);
			// so it doesn't get invalidated after effect is dead, and nothing
			// is displayed
			if(!(eff->active_&effect::ACTIVE)) {
				eff->active_ &= ~effect::INVALIDATE;
			}
		}
	}
}

void particle_engine::invalidate_effect(const effect& eff) {
	display::get_singleton()->invalidate_locations_in_rect(eff.surf_rect_);
}

// helper function to delete dynamically allocated effects from vector/list
void effect_remover(effect*& effp) {
	if(effp->active_&effect::KILL &&
			!(effp->active_&effect::INVALIDATE) &&
			!(effp->active_&effect::MODEL) ||
			(effp->active_&effect::INSTKILL))
	{
		delete effp;
		effp = NULL;
	}
}

void particle_engine::cleanout() {
	// STL style vector/list cleanout
	std::for_each(effect_list_.begin(), effect_list_.end(), effect_remover);
	std::list<effect*>::iterator end =
	std::remove(effect_list_.begin(), effect_list_.end(), static_cast<effect*>(NULL));
	effect_list_.erase(end, effect_list_.end());
}

effect* particle_engine::add_model_effect(const config& cfg,
										base_user& eu, bool model)
{
	effect* eff = new effect();
	std::string name = cfg["movement_name"];
	if(effect_config_.func_map.count(name) <= 0) {
		delete eff;
		std::cout<<name<<" not found"<<std::endl;
		return NULL;
	}
	eff->movement_f = &(effect_config_.func_map[name]);
	eff->particle_number_ = cfg["number"];
	eff->spread_ = cfg["spread"].to_double();
	eff->particle_lifetime_ = cfg["lifetime"];
	eff->emission_time_ = cfg["emit_time"];
	// in case emission time is not specified - to prevent div by 0
	eff->emission_time_ =
		eff->emission_time_ ? eff->emission_time_ : eff->particle_lifetime_;
	eff->lifetime_var_ = cfg["lifetime_var"];
	eff->follow_ = cfg["following"].to_bool();
	eff->particle_size_ = create_rect(
			0,
			0,
			cfg["particle_width"],
			cfg["particle_height"]);
	foreach(const std::string& col, utils::split(cfg["color"].str())) {
		eff->color_array_.push_back(lexical_hex_cast<Uint32>(col));
	}
	eff->active_ = effect::MODEL * model;
	eff->user_ = &eu;
	foreach (const config &conf, cfg.child_range("specific")) {
		eff->cfg = conf;
	}
	effect_list_.push_back(eff);
	return eff;
}

effect* particle_engine::copy_effect(effect& eff, base_user& eu) {
	effect* effp = new effect(eff);
	effp->user_ = &eu;
	effp->active_ = 0;
	effp->cfg = eff.cfg;
	effect_list_.push_back(effp);
	return effp;
}

inline bool particle_engine::effect_exists(effect* effp) {
	return std::count(effect_list_.begin(), effect_list_.end(), effp);
}

void particle_engine::move_effect(effect& eff, const map_location& loc) {
	display* disp = display::get_singleton();
	eff.update_particle_display_size();
	int x_new =
			disp->get_location_x(loc) +
			disp->screen_position_x() +
			disp->hex_size()/2;
	int y_new =
			disp->get_location_y(loc) +
			disp->screen_position_y() +
			disp->hex_size()/2;
	if(eff.follow_) {
		int dx = x_new - eff.origin_x;
		int dy = y_new - eff.origin_y;
		eff.origin_x = x_new;
		eff.origin_y = y_new;
		eff.shift_particles(dx, dy);
	}
	else {
		eff.origin_x = x_new;
		eff.origin_y = y_new;
	}
}

void particle_engine::move_effect(effect& eff, int x_off, int y_off) {
	display* disp = display::get_singleton();
	eff.update_particle_display_size();
	int x_new = disp->screen_position_x() + x_off;
	int y_new = disp->screen_position_y() + y_off;
	if(eff.follow_) {
		int dx = x_new - eff.origin_x;
		int dy = y_new - eff.origin_y;
		eff.origin_x = x_new;
		eff.origin_y = y_new;
		eff.shift_particles(dx, dy);
	}
	else {
		eff.origin_x = x_new;
		eff.origin_y = y_new;
	}
}

int particle_engine::remove_effect(effect& eff) {
	// ? do I really need this anymore?
	std::list<effect*>::iterator eit;
	for(eit = effect_list_.begin(); eit != effect_list_.end(); ++eit) {
		effect& e = **eit;
		if(&e == &eff) {
			eit = effect_list_.erase(eit);
			return 1;
		}
	}
	return 0;

}

void particle_engine::defer_move(effect& eff, const map_location& loc) {
	defer_info df(eff, loc);
	defer_list_.push_back(df);
	do_defers_ = true;

}

void particle_engine::apply_defers() {
	do_defers_ = false;
	foreach(const defer_info& df, defer_list_) {
		move_effect(*df.effp, df.loc);
	}
	defer_list_.clear();
}

void particle_engine::activate_effect(effect& eff) {
	eff.active_ |= effect::ACTIVE;
}

particle_engine* particle_engine::singleton_ = NULL;

int particle_engine::act_eff() {
	int nn = 0;
	foreach(effect*& eff, effect_list_) {
		if(eff->active_&effect::ACTIVE) {
			++nn;
		}
	}
	return nn;
}

#ifdef PRAND
#undef PRAND
#undef MATHPI
#endif
