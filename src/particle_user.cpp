/* $Id$ */
/*
   Copyright (C) 2012 by Justas Tomkus <justas.tomkus@gmail.com>
   Part of the Battle for Wesnoth Project http://www.wesnoth.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY.

   See the COPYING file for more details.
*/


#include "particle_user.hpp"
#include "particle_engine.hpp"
#include "config.hpp"
#include "particle_effect.hpp"
#include "foreach.hpp"

effect_user::effect_user() :
	base_user()
{
}

effect_user::effect_user(const config& cfg) :
	base_user()
{
	foreach (const config::any_child &fr, cfg.all_children_range()) {
		if("effect" == fr.key) {
			effect* eff = particle_engine::get_singleton()->
					add_model_effect(fr.cfg, *this);
			if(NULL != eff) {
				eff_list_.push_back(eff);
			}
		}
	}
}

effect_user::~effect_user() {
	remove_effect();
}

base_user::base_user() :
	eff_list_()
{
}

base_user::~base_user() {
}

int
base_user::remove_effect() {
	if(eff_list_.size() && particle_engine::get_singleton()) {
		foreach(effect*& eff, eff_list_) {
			eff->active_ |= effect::KILL;
		}
		// notify engine, that there are some effects requiring cleaning
		particle_engine::get_singleton()->collect_effects();
		return 1;
	}
	return false;
}

void
base_user::update_effect_position(int x_off, int y_off) {
	foreach(effect*& eff, eff_list_) {
		particle_engine::get_singleton()->move_effect(*eff, x_off, y_off);
	}
}
void
base_user::update_effect_position(const map_location& loc) {
	foreach(effect*& eff, eff_list_) {
		particle_engine::get_singleton()->move_effect(*eff, loc);
	}
}

void
base_user::start_effect() {
	foreach(effect*& eff, eff_list_) {
		particle_engine::get_singleton()->activate_effect(*eff);
	}
}

void
base_user::start_effect(const map_location& loc) {
	foreach(effect*& eff, eff_list_) {
		if(eff->active_&effect::MODEL) {
			eff = particle_engine::get_singleton()->
					copy_effect(*eff, *this);
		}
		particle_engine::get_singleton()->move_effect(*eff, loc);
		eff->active_ |= effect::ACTIVE;
	}
}

terr_eff_user::terr_eff_user() :
	base_user()
{
}

terr_eff_user::terr_eff_user(const terr_eff_user& eff) :
	base_user(eff)
{
}

terr_eff_user::~terr_eff_user() {
	foreach(effect*& effp, eff_list_) {
		effp->active_ |= effect::INSTKILL;
		particle_engine::get_singleton()->remove_effect(*effp);
	}
	eff_list_.clear();
	particle_engine::get_singleton()->collect_now();
}

void
terr_eff_user::add_effect(const config& cfg, const map_location& loc) {
	effect* eff = particle_engine::get_singleton()->
		add_model_effect(cfg, *this, false);
	if(NULL != eff) {
		eff_list_.push_back(eff);
		// try to defer move call somehow
		particle_engine::get_singleton()->defer_move(*eff, loc);
		eff->active_ |= effect::TERRAIN;
	}
}

void
terr_eff_user::stop_effect() {
	foreach(effect*& effp, eff_list_) {
		effp->active_ &= ~effect::ACTIVE;
	}
}


wml_effect_user::wml_effect_user() :
	base_user(),
	eff_list_()
{
	id_gen_ = 1;
};

wml_effect_user::~wml_effect_user() {
	// ---- delete associated effects
};

int
wml_effect_user::add_effect(const config& cfg, const map_location& loc, int id)
{
	effect* eff = particle_engine::get_singleton()->
		add_model_effect(cfg, *this, false);
	if(NULL != eff) {
		id = id ? id : id_gen_;
		while(1 == eff_list_.count(id)) {
			++id;
		}
		eff_list_[id] = eff;
		particle_engine::get_singleton()->move_effect(*eff, loc);
	}
	return id;
}
void
wml_effect_user::update_effect_position(const map_location& loc, int id) {
	if(1 == eff_list_.count(id)) {
		particle_engine::get_singleton()->move_effect(*(eff_list_[id]), loc);
	}
}

void
wml_effect_user::start_effect(int id) {
	// sanity check, if we actually have that effect
	if(1 == eff_list_.count(id)) {
		eff_list_[id]->active_ |= effect::ACTIVE;
	}
}

void
wml_effect_user::stop_effect(int id) {
	if(1 == eff_list_.count(id)) {
		eff_list_[id]->active_ &= ~effect::ACTIVE;
	}
}

int wml_effect_user::id_gen_ = 1;
