/* $Id$ */
/*
   Copyright (C) 2012 by Justas Tomkus <justas.tomkus@gmail.com>
   Part of the Battle for Wesnoth Project http://www.wesnoth.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY.

   See the COPYING file for more details.
*/

#include "particle_effect.hpp"
//#include "particle_engine.hpp"
#include "particle_user.hpp"
#include "display.hpp"
#include "foreach.hpp"

effect::effect() : follow_(false), part_to_emit_(0.0f)
{
	surf_rect_.w = 0;
	surf_rect_.h = 0;
}

effect::~effect() {
	foreach(particle*& partp, particle_list_) {
		delete partp;
	}
}

void effect::update_particle_display_size() {
	// this assumes default zoom factor of 1, only used when display is not
	// available yet
//	double zoom = display::get_singleton() ?
	double zoom = display::get_singleton()->get_zoom_factor();// : 1;
	// 0.5 is for rounding, works because w and h can only get positive values
	part_display_size_.w = static_cast<int>(0.5 + particle_size_.w * zoom);
	part_display_size_.h = static_cast<int>(0.5 + particle_size_.h * zoom);
}

// helper
void particle_remover(particle*& partp) {
	delete partp;
	partp = NULL;
}
void effect::populate_particles(int dt) {
	//premature kill off all particles (needed for moving effects)
	if((active_&KILL) && follow_) {
		std::for_each(particle_list_.begin(), particle_list_.end(), particle_remover);
		std::vector<particle*>::iterator end =
		std::remove(particle_list_.begin(), particle_list_.end(), static_cast<particle*>(NULL));
		particle_list_.erase(end, particle_list_.end());
	}
	// when effect_user dies and/or issues a kill, wait for particles to expire
	// and then remove them. This prevents removing all particles at once.
	if((active_&KILL) || particle_list_.size() > particle_number_) {
		std::vector<particle* >::iterator pit;
		for(pit = particle_list_.begin(); pit != particle_list_.end(); ++pit) {
			if((*pit)->Time() > particle_lifetime_) {
				delete *pit;
				particle_list_.erase(pit);
				if(!(particle_list_.size())) {
					active_ &= ~ACTIVE;
				}
				return;
			}
		}
		// wait for some particles to expire
		return;
	}
	// respawn expired particles at the spawn point
	std::vector<particle* >::iterator pit;
	for(pit = particle_list_.begin(); pit != particle_list_.end(); ++pit) {
		if((*pit)->Time() > particle_lifetime_) {
			(*pit)->respawn(origin_x, origin_y, *this);
		}
	}
	// fill list with particles, if no particle is respawned and we can add more
	// particles to list
	if(particle_list_.size() < particle_number_) {
		part_to_emit_ +=
			static_cast<float>(dt*particle_lifetime_)/emission_time_;
		while(part_to_emit_ > 1.0 &&
					particle_list_.size() < particle_number_) {
			populate_particles(origin_x, origin_y);
			particle_list_.back()->respawn(origin_x, origin_y, *this);
			part_to_emit_ -= 1;
		}
		part_to_emit_ -= static_cast<int>(part_to_emit_);
		return ;
	}

}
void effect::shift_particles(int dx, int dy) {
	foreach(particle*& part, particle_list_) {
		part->shift(dx, dy);
	}
}

void effect::populate_particles(int x, int y) {
		particle_list_.push_back(new particle(x, y, color_array_.front()));
		particle_list_.back()->recreate_surface(
				particle_size_.w,
				particle_size_.h);
}

